﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
   public interface IBL
    {
        bool userLogIn(string userName, string password);
        bool passwordInputIsValid(string userName, string password);
        string generateValidRandomPassword();
        void changePassword(string password);
    }
}
