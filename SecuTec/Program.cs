﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using PL;
using BL;
namespace SecuTec
{
    class Program
    {
        static void Main(string[] args)
        {
            // comment to check git activity
            IDAL DALIns = new DAL_List();
            IBL BLIns = new BL_v1(DALIns);
            IPL PLIns = new PL_CLI(BLIns);
            PLIns.run();
        }
    }
}
